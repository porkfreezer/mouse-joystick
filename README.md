# Mouse Joystick

Create a virtual joystick from multiple mice and keyboards using Linux libevdev.

Requires Python bindings for `libevdev` and `yaml`, and permissions to access input devices and create new `uinput` devices.

### List available input devices
`$ mouse_joystick -l`

Example output:
```
==== event10 ====
 aka  /dev/input/by-path/platform-AMDI0010:03-event-mouse
Name: ELAN0718:00 04F3:30FD Touchpad
Bus: 24 Vendor: 04f3 Product: 30fd Version: 256

EV_KEY:  BTN_LEFT BTN_TOOL_FINGER BTN_TOOL_QUINTTAP BTN_TOUCH
BTN_TOOL_DOUBLETAP BTN_TOOL_TRIPLETAP BTN_TOOL_QUADTAP

EV_ABS:  ABS_X ABS_Y ABS_MT_SLOT ABS_MT_POSITION_X ABS_MT_POSITION_Y
ABS_MT_TOOL_TYPE ABS_MT_TRACKING_ID
```

Devices can always be referenced from `/dev/input/eventXX`, but these locations may change after reboots. `/dev/input/by-id/` and `/dev/input/by-path` have symlinks that are more reliable. For USB devices, `by-id` is probably the best place (unless you have multiple identical devices), since plugging the same device into a different port will yield a different path. Most other devices won't have an entry in `by-id`, but their entries in `by-path` probably won't ever change.

Some devices may have multiple event devices associated with them. For example, my wireless USB mouse has a separate device with a single `ABS_MISC` axis that does something when the mouse connects. Check which axes and buttons are available from each event to determine which one to use.

### Create a configuration

See `example.yaml` for configuration details.

### Create the virtual joystick

`$ mouse_joystick -f <config>.yaml`


`mouse_joystick` is released under the terms of the MIT license:
```
Copyright (c) 2024 Park Frazer

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
```