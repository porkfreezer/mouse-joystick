#!/usr/bin/env python3

# mouse_joystick - Create a virtual joystick from other input devices
#
# Copyright (c) 2024 Park Frazer
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import libevdev
import fcntl
import os
import yaml
import argparse
from select import select

# Output axis range. 32767 is the max for 16-bit event values.
AXIS_MAX = 32767

class OutputDevice:
  """ Virtual uinput output device """
  # Enable the next available button, returning the associated code
  def add_button(self):
    while True:
      # keep within joystick button range to avoid pressing real keys
      if self.button_code == libevdev.evbit('BTN_THUMBR').value:
        self.button_code = libevdev.evbit('BTN_TRIGGER_HAPPY1').value
      else:
        self.button_code += 1
      code = libevdev.evbit('EV_KEY', self.button_code)
      if code.value > libevdev.evbit('BTN_TRIGGER_HAPPY40').value:
        print("Too many buttons")
        exit(-1)
      if code.is_defined:
        break
    self.device.enable(code)
    return code

  # Enable the next available absolute axis, returning the associated code
  def add_abs(self):
    while True:
      self.abs_code += 1
      code = libevdev.evbit('EV_ABS', self.abs_code)
      if code.value > libevdev.evbit('ABS_HAT3Y').value:
        print("Too many axes")
        exit(-1)
      if code.is_defined:
        break
    self.device.enable(code,
             libevdev.InputAbsInfo(minimum=-AXIS_MAX, maximum=AXIS_MAX))
    return code

  # Add an event to an outgoing queue without actual sending it
  def send_event(self, code, value):
    self._buf.append(libevdev.InputEvent(code, value=value))

  # Send buffered events with a SYN
  def flush_events(self):
    self._buf.append(libevdev.InputEvent(libevdev.EV_SYN.SYN_REPORT, value=0))
    self.uinput.send_events(self._buf)
    self._buf = []

  # Create virtual device after initializing necessary outputs
  def start(self):
    self.uinput = self.device.create_uinput_device()
    print(f"Created '{self.device.name}' at {self.uinput.devnode}")

  def __init__(self, name):
    self.device = libevdev.Device()
    self.device.name = name
    self.button_code = libevdev.evbit('BTN_TRIGGER').value-1
    self.abs_code = libevdev.evbit('ABS_X').value-1
    self._buf = []


class AbsAxis:
  """ Directly-mapped absolute axis """
  def map_value(self, value):
    value = round(((value - self.in_min) / self.in_range * 2 - 1)*AXIS_MAX)
    return -value if self.invert else value

  # Return true if the input event was for this axis and send the output event
  def handle_event(self, event):
    if event.matches(self.code):
      self.value = self.map_value(event.value)
      self.outdev.send_event(self.out_code, self.value)
      return True
    return False

  def __init__(self, code, absinfo, invert, outdev):
    self.code = code
    self.in_min = absinfo.minimum
    self.in_range = absinfo.maximum - absinfo.minimum
    self.invert = invert
    self.value = self.map_value(absinfo.value)
    self.outdev = outdev
    self.out_code = outdev.add_abs()

class RelAxis:
  """ Scaled relative to absolute axis """
  def handle_event(self, event):
    if event.matches(self.code):
      self.value += event.value * self.scale
      self.value = max(-AXIS_MAX, min(self.value, AXIS_MAX))
      val = round(-self.value if self.invert else self.value)
      self.outdev.send_event(self.out_code, val)
      return True
    return False

  def __init__(self, code, scale, invert, outdev):
    self.code = code
    self.scale = scale
    self.invert = invert
    self.value = 0
    self.outdev = outdev
    self.out_code = outdev.add_abs()

class Button:
  """ Directly mapped button """
  def handle_event(self, event):
    if event.matches(self.code):
      self.pressed = event.value != 0
      self.outdev.send_event(self.out_code, 1 if self.pressed else 0)
      return True
    return False

  def __init__(self, code, outdev):
    self.code = code
    self.pressed = False
    self.outdev = outdev
    self.out_code = outdev.add_button()

class ToggleButton:
  """ Converts standard button to toggle button """
  def handle_event(self, event):
    if event.matches(self.code):
      old = self.input_pressed
      self.input_pressed = event.value != 0
      if self.input_pressed and not old:
        self.pressed = not self.pressed
        self.outdev.send_event(self.out_code, 1 if self.pressed else 0)
      return True
    return False

  def __init__(self, code, outdev):
    self.code = code
    self.pressed = False
    self.input_pressed = False
    self.outdev = outdev
    self.out_code = outdev.add_button()

class InputDevice:
  """ Stores configuration and axes for one input device """

  def error(self, msg):
    print(f"In '{self.device.name}': {msg}")
    exit(-1)

  @property
  def paused(self):
    return any(x.pressed for x in self.pause)

  def _handle_event(self, event, paused):
    for a in self.pause:
      if a.handle_event(event):
        return
    if not paused:
      for a in self.axes:
        if a.handle_event(event):
          return
      for a in self.buttons:
        if a.handle_event(event):
          return

  # Process any events from this input device
  def handle_events(self, paused):
    if self.grab:
      if paused:
        self.device.ungrab()
      else:
        self.device.grab()

    for event in self.device.events():
      self._handle_event(event, paused)


  def _init_axes(self, config, outdev):
    for i in config:
      axis_config = config[i]
      code = libevdev.evbit(i)
      if code.type != libevdev.EV_REL and code.type != libevdev.EV_ABS:
        self.error(f"{i} is not an axis")
      if not self.device.has(code):
        self.error(f"device does not have {i}")

      scale = 1
      invert = False
      if axis_config:
        for i in axis_config:
          if i == 'scale' and code.type == libevdev.EV_REL:
            scale = axis_config[i]
          elif i == 'invert':
            invert = axis_config[i]
          else:
            self.error(f"unknown axis property '{i}'")

      if code.type == libevdev.EV_REL:
        self.axes.append(RelAxis(code, scale, invert, outdev))
      else:
        absinfo = self.device.absinfo[code]
        self.axes.append(AbsAxis(code, absinfo, invert, outdev))

  def _init_buttons(self, config, outdev):
    for i in config:
      btn_config = config[i]
      code = libevdev.evbit(i)
      if code.type != libevdev.EV_KEY:
        self.error(f"{i} is not a button/key")
      if not self.device.has(code):
        self.error(f"device does not have {i}")

      toggle = False
      pause = False
      if btn_config:
        for i in btn_config:
          if i == 'toggle':
            toggle = btn_config[i]
          elif i == 'pause':
            pause = btn_config[i]
          else:
            self.error(f"unknown button property '{i}'")

      l = self.pause if pause else self.buttons
      if toggle:
        l.append(ToggleButton(code, outdev))
      else:
        l.append(Button(code, outdev))

  # Attach to fd, the input device, using the given configuration.
  # Send output events to outdev.
  def __init__(self, fd, config, outdev):
    self.device = libevdev.Device(fd)

    self.axes = []
    self.buttons = []
    self.pause = []
    self.grab = False

    for i in config:
      if i == 'axes':
        self._init_axes(config[i], outdev)
      elif i == 'buttons':
        self._init_buttons(config[i], outdev)
      elif i == 'grab':
        self.grab = True
      else:
        self.error(f"unknown property '{i}")

# Print all codes of a type that the device supports
def list_inputs(device, typ):
  if device.has(libevdev.evbit('EV_'+typ)):
    print(f'\nEV_{typ}:  ', end='')
    for i in range(libevdev.evbit(typ+'_MAX').value):
      code = libevdev.evbit('EV_'+typ, i)
      if device.has(code):
        print(code.name, end=' ')
    print()

# Find events on a
def list_devices():
  path_dir = '/dev/input/by-path'
  id_dir = '/dev/input/by-id'
  bypath = {}
  byid = {}

  if os.path.exists(path_dir):
    for i in os.listdir(path_dir):
      fullpath = os.path.join(path_dir, i)
      target = os.path.basename(os.readlink(fullpath))
      if target[0:5] == 'event':
        bypath[target] = fullpath

  if os.path.exists(id_dir):
    for i in os.listdir(id_dir):
      fullpath = os.path.join(id_dir, i)
      target = os.path.basename(os.readlink(fullpath))
      if target[0:5] == 'event':
        byid[target] = fullpath

  events = [x for x in os.listdir('/dev/input') if x[0:5] == 'event']
  events.sort(key=lambda s : int(s[5:]))
  for ev in events:
    print(f"==== {ev} ====")
    if ev in byid:
      print(' aka ', byid[ev])
    if ev in bypath:
      print(' aka ', bypath[ev])

    fd = open(os.path.join('/dev/input', ev))
    device = libevdev.Device(fd)
    print(f'Name: {device.name}')
    devid = device.id
    print(f"Bus: {devid['bustype']} Vendor: {devid['vendor']:04x} Product: {devid['product']:04x} Version: {devid['version']}")

    list_inputs(device, 'KEY')
    list_inputs(device, 'ABS')
    list_inputs(device, 'REL')
    print("\n")

def run(config_file):
  with open(config_file) as stream:
    try:
      config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
      print(exc)
      exit(-1)

  if not 'devices' in config:
    print("No input devices specified")
    exit(-1)

  if 'name' in config:
    name = config['name']
  else:
    name = "Mouse Joystick"

  outdev = OutputDevice(name)

  fds = []
  devices = []
  for path in config['devices']:
    try:
      fd = open(path, 'rb')
    except IOError as e:
      print(f"Failed to open '{path}': {e.strerror}")
      exit(-1)
    fcntl.fcntl(fd, fcntl.F_SETFL, os.O_NONBLOCK)
    fds.append(fd)
    devices.append(InputDevice(fd, config['devices'][path], outdev))

  outdev.start()
  paused = False

  try:
    while True:
      select(fds, [], [])
      next_paused = False
      for d in devices:
        d.handle_events(paused)
      outdev.flush_events()
      paused = any(d.paused for d in devices)

  except KeyboardInterrupt:
    print("\rInterrupted")
    exit(0)


parser = argparse.ArgumentParser(description='Combine mice or other input devices into one virtual joystick.')
parser.add_argument('-f', '--file', metavar='CONFIG', help='device configuration file to load or to save interactive configuration to')
parser.add_argument('-l', '--list-devices', action='store_true', help='list available devices with their axes and buttons')
args = parser.parse_args()

if args.list_devices:
  list_devices()
  exit(0)

if not args.file:
  parser.print_help()
  print("\nno configuration file specified")
  exit(-1)

run(args.file)